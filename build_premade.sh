#!/bin/sh

# Build premade resflash images from -stable sets
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details
# prerequisite (root): `PKG_PATH=https://cdn.openbsd.org/%m/ pkg_add -vi xxhash`
# prerequisite: sets in /usr/rel
# sample usage: su root -c './build_premade.sh /home/bconway/resflash'

set -o errexit -o nounset -o pipefail #-o xtrace # DEBUG

if [ ${#} -ne 1 ]; then
  echo "Usage: ${0} resflash_dir"
  exit 1
else
  RESFLASHBIN=${1}
fi

MACHINE=$(machine)
DESTDIR=/usr/rdest
RELDIR=/usr/rel
SETS='base man'
KERNEL=bsd.mp
FW=
COM0=115200
tst=$(date +%s)

case ${MACHINE} in
  amd64) ;;
  i386) KERNEL=bsd
        FW=-u
        COM0=38400;;
  arm64) ;;
  *) echo 'Unsupported arch.'
     exit 1;;
esac

# Clean old images

echo '----- Cleaning old images -----'
rm -rf premade ${DESTDIR} premade resflash-*.{fs,img}*

# Make base dir

echo -n '----- Making base dir -----'
st=$(date +%s)
mkdir ${DESTDIR}
cp ${RELDIR}/${KERNEL} ${DESTDIR}/bsd
cp ${RELDIR}/bsd.rd ${DESTDIR}/
for set in ${SETS}; do
  tar zxfph ${RELDIR}/${set}??.tgz -C ${DESTDIR}
done
# Add on non-set
tar zxfph ${DESTDIR}/var/sysmerge/etc.tgz -C ${DESTDIR}
rm ${DESTDIR}/usr/share/relink/kernel.tgz
echo " ($(($(date +%s) - st))s)"

# Build images
mkdir -p premade/{install,upgrade}

compress_images() {
  echo '----- Compressing images and populating premade -----'
  st=$(date +%s)
  for image in resflash-${MACHINE}-*.img; do
    echo ${image}
    gzip -4 ${image}
    mv ${image}.gz premade/install/
  done

  for image in resflash-${MACHINE}-*.fs; do
    echo ${image}
    gzip -4 ${image}
    mv ${image}.gz premade/upgrade/
  done
  echo "----- Compressing images complete ----- ($(($(date +%s) - st))s)"
}

# 1998 MB (not MiB, rounded to 1024)
if [ ${MACHINE} != 'arm64' ]; then
  echo '----- Building VGA image -----'
  ${RESFLASHBIN}/build_resflash.sh ${FW} 1906 ${DESTDIR}
  compress_images
fi

echo '----- Building com0 image -----'
${RESFLASHBIN}/build_resflash.sh ${FW} -s ${COM0} 1906 ${DESTDIR}
compress_images

echo -n '----- Calculating checksums -----'
st=$(date +%s)
mv *.fs.*sum premade/upgrade/

(
  cd premade
  find install/ upgrade/ -type f|sort|xargs cksum -a sha512 -h SHA512
  if which xxhsum > /dev/null 2>&1; then
    # Pass -q to xxhsum to prevent it from clearing the line
    find install/ upgrade/ -type f|sort|xargs xxhsum -H3 -q >> XXH3
  fi
)
echo " ($(($(date +%s) - st))s)"

echo "----- Build complete! ----- ($(($(date +%s) - tst))s)"
