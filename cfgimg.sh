#!/bin/sh

# Mount and configure resflash .img or .fs file
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details
# prerequisite: stock resflash image
# sample usage:
# su root -c './cfgimg.sh /home/bconway/resflash resflash-*_*.img rtest'

set -o errexit -o nounset -o pipefail #-o xtrace # DEBUG

BYTESECT=512
tst=$(date +%s)

if [ ${#} -ne 3 ]; then
  echo "Usage: ${0} resflash_dir resflash_img_or_fs short_hostname"
  exit 1
else
  RESFLASHBIN=${1}
  ORIGFILE=${2}
  CFGHOST=${3}
fi

ORIGSUFF=$(echo ${ORIGFILE}|awk -F . -safe '{ print $NF }')
IMGFILE=resflash-cfgimg-${CFGHOST}.${ORIGSUFF}

# Source resflash.sub based on image location
. ${RESFLASHBIN}/resflash.sub
set_attr_by_machine $(machine)

echo '----- Cleaning old images -----'
rm -f resflash-cfgimg-*.{fs,img}*
sync

echo -n "----- Creating new image ${IMGFILE} -----"
st=$(date +%s)
cp -p ${ORIGFILE} ${IMGFILE}
echo " ($(($(date +%s) - st))s)"

# Assigns: MNTPATH
mount_img_or_fs ${IMGFILE}
df -h|grep ${MNTPATH}

# Common (/)

echo '----- Configuring / -----'

# Host-specific (/)

if [ -f ./cfgimg-root-${CFGHOST}.sh ]; then
  . ./cfgimg-root-${CFGHOST}.sh
fi

# Common (/var)

echo '----- Configuring /var -----'

# Host-specific (/var)

if [ -f ./cfgimg-var-${CFGHOST}.sh ]; then
  . ./cfgimg-var-${CFGHOST}.sh
fi

# Common (/etc)

echo '----- Configuring /etc -----'
echo "${CFGHOST}.example.com" > ${MNTPATH}/fs/etc/myname

echo '127.0.0.1 localhost' > ${MNTPATH}/fs/etc/hosts
echo '::1 localhost' >> ${MNTPATH}/fs/etc/hosts

ln -sf /usr/share/zoneinfo/UTC ${MNTPATH}/fs/etc/localtime

echo 'sndiod_flags=NO' > ${MNTPATH}/fs/etc/rc.conf.local

echo 'lookup file bind' > ${MNTPATH}/fs/etc/resolv.conf

# Host-specific (/etc)

if [ -f ./cfgimg-etc-${CFGHOST}.sh ]; then
  . ./cfgimg-etc-${CFGHOST}.sh
fi

# Common (/root)

echo '----- Configuring /root -----'

# Common (chroots)

echo '----- Running chroots -----'
chroot ${MNTPATH}/fs useradd -m -b /home -c "My User" myuser
chroot ${MNTPATH}/fs usermod -G wheel myuser
chmod 700 ${MNTPATH}/fs/home/myuser
echo myuser > ${MNTPATH}/fs/root/.forward

# Host-specific (chroots)

if [ -f ./cfgimg-chroot-${CFGHOST}.sh ]; then
  . ./cfgimg-chroot-${CFGHOST}.sh
fi

# Clean up
umount_all
rm -r ${MNTPATH}

if [ ${ORIGSUFF} == 'fs' ]; then
  echo -n '----- Calculating checksums -----'
  st=$(date +%s)
  cksum -a ${ALG} -h ${IMGFILE}.cksum ${IMGFILE}

  if [ -n "${XXHALG+1}" ]; then
    # Pass -q to xxhsum to prevent it from clearing the line
    xxhsum -H${XXHALG} -q ${IMGFILE} > ${IMGFILE}.xxhsum
  fi
  echo " ($(($(date +%s) - st))s)"

  cat ${IMGFILE}.*sum
fi

echo "----- Configuration complete! ----- ($(($(date +%s) - tst))s)"
